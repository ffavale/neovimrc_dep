set nocompatible
set encoding=UTF-8

set showmatch
set ignorecase

set hlsearch
set incsearch

set number
set relativenumber

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smarttab
set scrolloff=16
set cmdheight=1

set wildmode=longest,list
set colorcolumn=128
highlight colorcolumn ctermbg=0 guibg=lightgrey
set signcolumn=yes

set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set autoread

filetype plugin indent on
syntax on

set mouse=v
set guicursor=

" :set cursorline

" PLUGINS

call plug#begin()

Plug 'powerline/powerline'
Plug 'ryanoasis/vim-devicons'
Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'

Plug 'raimondi/delimitmate'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'tpope/vim-fugitive'

Plug 'flazz/vim-colorschemes'
Plug 'sheerun/vim-polyglot'
Plug 'voldikss/vim-floaterm'
Plug 'uiiaoo/java-syntax.vim'

Plug 'majutsushi/tagbar'
call plug#end()

let g:airline_powerline_fonts = 1
let g:airline_theme='jellybeans'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

let g:NERDTreeGitStatusShowClean = 1
let g:NERDTreeGitStatusUseNerdFonts = 1
silent! nmap <C-p> :NERDTreeToggle<CR>
silent! map <F3> :NERDTreeFind<CR>
map <F2> :NERDTreeToggle<CR>

let g:NERDTreeMapActivateNode="<F3>"
let g:NERDTreeMapPreview="<F4>"

let g:NERDCreateDefaultMappings = 1
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1

let g:coc_global_extensions = ['coc-lists', 'coc-java', 'coc-json', 'coc-lightbulb', 'coc-omnisharp', 'coc-pyright', 'coc-rome', 'coc-rls', 'coc-r-lsp', 'coc-sh', 'coc-sql', 'coc-solargraph', 'coc-svg', 'coc-texlab', 'coc-clangd', 'coc-cmake', 'coc-css', 'coc-go', 'coc-html']

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

let g:floaterm_width = 0.8
let g:floaterm_height = 0.8

nnoremap   <silent>   <F7>    :FloatermNew<CR>
tnoremap   <silent>   <F7>    <C-\><C-n>:FloatermNew<CR>
nnoremap   <silent>   <F8>    :FloatermPrev<CR>
tnoremap   <silent>   <F8>    <C-\><C-n>:FloatermPrev<CR>
nnoremap   <silent>   <F9>    :FloatermNext<CR>
tnoremap   <silent>   <F9>    <C-\><C-n>:FloatermNext<CR>
nnoremap   <silent>   <F12>   :FloatermToggle<CR>
tnoremap   <silent>   <F12>   <C-\><C-n>:FloatermToggle<CR>

nmap <F6> :TagbarToggle<CR>
nmap <F5> :FloatermNew ./make.sh && echo "" && read -n 1 -s -r -p "Press any key to continue..."<CR>
nmap <F4> :FloatermNew ./debug.sh && echo "" && read -n 1 -s -r -p "Press any key to continue..."<CR>

let mapleader=" "
colorscheme molokai
set background=dark

highlight link javaIdentifier NONE

